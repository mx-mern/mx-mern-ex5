const express = require('express');
const app = express();
const port = 3000;
const bodyparser = require('body-parser');

const router = require('./user')

app.use(bodyparser.json());

app.use('/user', router);


app.listen(port, () => console.log(`Mx-Mern-Ex5 app listening on port ${port}!`))