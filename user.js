const express = require('express');
const fs = require('fs');
const fileName = "./data.json"
const datafile = require(fileName);
const router = express.Router();
const stringSimilarity = require('string-similarity');

var nextId = 0;
for (data of datafile) {
    if (data.id) {
        if (data.id > nextId) {
            nextId = data.id;
        }
    }
}
nextId += 1;
console.log('nextID = ', nextId);

router.get('/list', function (request, response) {
    var items = [];
    for (item of datafile) {
        if (item.username) {
            items.push(item.username);
        }
    }
    response.json({ data: items, success: true });
})

router.get('/find', function (request, response) {
    const query = request.query.q;
    console.log('query: ', query, 'datafile:', datafile);
    var matchUser = null;
    var similarity = 0;
    for (item of datafile) {
        if (item.username) {
            var newSimlarity = stringSimilarity.compareTwoStrings(item.username, query);
            if (newSimlarity > similarity) {
                matchUser = item;
                newSimlarity = similarity;
            }
        }
    }
    if (matchUser) {
        response.json({ data: matchUser, success: true });
    } else {
        response.json({ message: "User not exist", success: false });
    }
});

router.post('/create', function (request, response) {
    const username = request.body.username;
    const password = request.body.password;
    if (username && password) {
        item = { id: nextId, username: username, password: password };
        nextId += 1;
        datafile.push(item);
        console.log("new data file: ", datafile);
        fs.writeFile(fileName, JSON.stringify(datafile),
            function (err, data) {
                if (err) {
                    response.json({ error: err, success: false });
                } else {
                    response.json({ user: username, success: true });
                }
            });
    } else {
        response.json({ error: "Missing username or password", success: false });
    }
});

router.put('/update/:id', function (request, response) {
    const id = request.params.id;
    if (id) {
        const result = datafile.find(function (item) {
            return item.id == id;
        });
        if (result) {
            const username = request.body.username;
            if (username) {
                var oldname = result.username;
                result.username = username;
                fs.writeFile(fileName, JSON.stringify(datafile),
                    function (err, data) {
                        if (err) {
                            response.json({ error: err, success: false });
                        } else {
                            response.json({ id: result.id, oldname: oldname, user: username, success: true });
                        }
                    });
            } else {
                response.json({ error: "Missing username", success: false });

            }
        }
    }
});


router.put('/delete/:id', function (request, response) {
    const id = request.params.id;
    if (id) {
        const result = datafile.findIndex(function (item) {
            return item.id == id;
        });
        if (result >= 0) {
            var delUser = datafile.splice(result, 1);
            console.log("new datafile: ", datafile);
            fs.writeFile(fileName, JSON.stringify(datafile),
                function (err, data) {
                    if (err) {
                        response.json({ error: err, success: false });
                    } else {
                        response.json({ data: delUser, success: true });
                    }
                });
        } else {
            response.json({ error: "No id found", success: false });

        }
    }

});
module.exports = router;